(function() {
    var app = angular.module("minApp");

    var DivisjonInfoController = function($scope, DivisjonInfoService) {

        var hentDivisjonInfo = function() {
            DivisjonInfoService.hentDivisjonInfo();
        };

        if(!$scope.divisjonInfo) {
            hentDivisjonInfo();
        }
    };

    app.controller("DivisjonInfoController", DivisjonInfoController);
}());
