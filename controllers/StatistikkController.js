(function() {
    var app = angular.module("minApp");

    var StatistikkController = function($scope, StatistikkService) {

        // Bruker StatistikkService
        var hentAntallSpillerePerSesong = function() {
            StatistikkService.hentAntallSpillerePerSesong();
        };

        var hentAntallUavgjortPerSesong = function() {
            StatistikkService.hentAntallUavgjortPerSesong();
        };

        var hentGenerellStatistikkPerSesong = function() {
            StatistikkService.hentGenerellStatistikkPerSesong();
        };

        if(!$scope.antallSpillerePerSesong) {
            hentAntallSpillerePerSesong();
        }

        if(!$scope.antallUavgjortPerSesong) {
            hentAntallUavgjortPerSesong();
        }

        if(!$scope.antallDivisjonerPerSesong) {
            hentGenerellStatistikkPerSesong();
        }
    };

    app.controller("StatistikkController", StatistikkController);
}());
