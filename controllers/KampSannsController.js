(function() {
    var app = angular.module("minApp");

    var KampSannsController = function($scope, KampSannsService) {

        //Bruker KampSannsService
        var hentKampSanns = function(navn1, navn2) {
          KampSannsService.hentKampSanns(navn1, navn2);
        };

        if(!$scope.kampSanns) {
            hentKampSanns("skwais", "Tommoen");
        }

    };

    app.controller("KampSannsController", KampSannsController);
}());
