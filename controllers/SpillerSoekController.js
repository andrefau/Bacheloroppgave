(function() {
    var app = angular.module("minApp");

    var SpillerSoekController = function($scope, SpillerSoekService) {

        // Bruker SpillerSoekService
        $scope.search = function(username) {
            SpillerSoekService.spillerSoek(username);
        };
    };

    app.controller("SpillerSoekController", SpillerSoekController);
}());
