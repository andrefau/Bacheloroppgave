(function() {
    var app = angular.module("minApp");

    var RankingController = function($scope, RankingService) {

        // Bruker RankingService
        var hentRank = function() {
            RankingService.hentRank();
        };

        if(!$scope.spillereRank) {
            hentRank();
        }
    };

    app.controller("RankingController", RankingController);
}());
