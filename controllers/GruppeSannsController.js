(function() {
    var app = angular.module("minApp");

    var GruppeSannsController = function($scope, GruppeSannsService) {

        //Bruker GruppeSannsService
        var hentGruppeSanns = function(navn) {
          GruppeSannsService.hentGruppeSanns(navn);
        };

        if(!$scope.gruppeSanns) {
            hentGruppeSanns("skwais");
        }
    };

    app.controller("GruppeSannsController", GruppeSannsController);
}());
