(function() {
    var app = angular.module("minApp", ["ui.bootstrap", "ngAnimate", "googlechart",
    "angular-loading-bar", "ngRoute"]);

    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/ranking', {
                    templateUrl: 'views/RankingView.html',
                    controller: 'RankingController'
                }).
                when('/antallspillerepersesong', {
                    templateUrl: 'views/AntallSpillerePerSesongView.html',
                    controller: 'StatistikkController'
                }).
                when('/antalluavgjortpersesong', {
                    templateUrl: 'views/AntallUavgjortPerSesongView.html',
                    controller: 'StatistikkController'
                }).
                when('/sesonginfo', {
                    templateUrl: 'views/SesongInfoView.html',
                    controller: 'StatistikkController'
                }).
                when('/gruppesanns', {
                    templateUrl: 'views/GruppeSannsView.html',
                    controller: 'GruppeSannsController'
                }).
                when('/kampsanns', {
                    templateUrl: 'views/KampSannsView.html',
                    controller: 'KampSannsController'
                }).
                when('/spillerinfo', {
                    templateUrl: 'views/SpillerSoekView.html',
                    controller: 'SpillerSoekController'
                }).
                when('/hjem', {
                    templateUrl: 'views/Hjem.html',
                    controller: 'MainController'
                }).
                when('/spillerdivisjon', {
                    templateUrl: 'views/SpillerDivisjon.html',
                    controller: 'SpillerSoekController'
                }).
                when('/divisjoninfo', {
                    templateUrl: 'views/DivisjonInfoView.html',
                    controller: 'DivisjonInfoController'
                }).
                otherwise({
                    redirectTo: '/hjem'
                });
        }]);
}());
