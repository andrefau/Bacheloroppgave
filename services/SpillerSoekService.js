(function() {
  var SpillerSoekService = function($rootScope, $location, $log, RestService, googleChartApiPromise) {

    /*var lagStringKampResultater = function(data) {
        var string = '{' +
              '"cols": [' +
                    '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
                    '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
                    '{"id":"uavgjort", "label":"Uavgjort", "type":"number", "p":{}},' +
                    '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}}' +
                  '],' +
             ' "rows": [';
        for (var i in data) {
            string += '{"c":[{"v":"' +data[i].Sesong+ '"},{"v":' +data[i].Vunnet+
            '},{"v":' +data[i].Uavgjort+ '},{"v":' +data[i].Tapt+ '},null]},';
        }
        $rootScope.antallSesonger = data.length;
        string = string.slice(0, -1);
        string += ']}';
        return string;
    };*/

    var lagStringRanking = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"rank", "label":"Rank", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Rank + '},null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringAntallKamperOgSeiere = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"antall seiere", "label":"Antall seiere", "type":"string", "p":{}},' +
        '{"id":"antall kamper", "label":"Antall kamper", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var antallKamper = 0;
      var antallSeiere = 0;
      for (var i in data) {
        antallKamper += (data[i].Vunnet + data[i].Tapt + data[i].Uavgjort);
        antallSeiere += (data[i].Vunnet);
        string += '{"c":[{"v":"' + antallSeiere + '"},{"v":' + antallKamper + '},null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultater = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
        '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}},' +
        '{"id":"uavgjort", "label":"Uavgjort", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Vunnet +
          '},{"v":' + data[i].Tapt + '},{"v":' + data[i].Uavgjort + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultaterUUavgjort = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
        '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Vunnet +
          '},{"v":' + data[i].Tapt + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKamper = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"antall kamper", "label":"Antall kamper", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        var antall = data[i].Vunnet + data[i].Tapt + data[i].Uavgjort;
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + antall + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultaterCombo = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"vunnet", "label":"Vunnet", "type":"number", "p":{}},' +
        '{"id":"tapt", "label":"Tapt", "type":"number", "p":{}},' +
        '{"id":"total", "label":"Total", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "0" +
            '},{"v":' + "0" + '},{"v":' + "0" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Vunnet +
          '},{"v":' + data[i].Tapt + '},{"v":' + (data[i].Tapt + data[i].Uavgjort + data[i].Vunnet) + '},null]},';
        teller++;
      }
      $rootScope.antallSesonger = data.length;
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    /**
    var tabell = new Array(9);
    for (var i = 0; i < tabell.length; i++) {
      tabell[i] = 0;
    }
    for (var i = 0; i < data.length; i++) {
      var divisjon = data[i].Divisjon;
      $log.info("Index: " + (divisjon-1));
      tabell[divisjon-1] += 1;
      $log.info("Antall: " + tabell[divisjon-1]);
    }
    for (var i = 0; i < tabell.length; i++) {
      $log.info("divisjon: " + (i+1) + ", antall forekomster: " + tabell[i]);
      if (tabell[i] != 0) {
        var forekomsterDivisjon = tabell[i];
        var divisjon = i+1;
        string += '{"c":[{"v":"Divisjon ' +divisjon+ '"},{"v":' +forekomsterDivisjon+
        '},null]},';
      }
    */

    /*var divisjonYVerdier = [];
    var lagStringDivisjoner = function(data) {
      divisjonYVerdier = [];
        var string = '{' +
              '"cols": [' +
                    '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
                    '{"id":"divisjon", "label":"Divisjon", "type":"number", "p":{}}],' +
             ' "rows": [';
        for (var i in data) {
            string += '{"c":[{"v":"' +data[i].Sesong+ '"},{"v":' +data[i].Divisjon+
            '},null]},';*/
    /**
    if(divisjonYVerdier.indexOf(data[i].Divisjon)==-1) {
      divisjonYVerdier.push(data[i].Divisjon);
    }
    */
    /*}
            string = string.slice(0, -1);
            string += ']}';
            //divisjonYVerdier.sort();
            return string;
        };*/

    var lagStringDivisjoner = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"sesong", "label":"Sesong", "type":"string", "p":{}},' +
        '{"id":"divisjon", "label":"Divisjon", "type":"number", "p":{}}],' +
        ' "rows": [';
      var teller = data[0].Sesong;
      for (var i = 0; i < data.length; i++) {
        while (teller != data[i].Sesong) {
          string += '{"c":[{"v":"' + teller + '"},{"v":' + "null" + '},null]},';
          teller++;
        }
        string += '{"c":[{"v":"' + data[i].Sesong + '"},{"v":' + data[i].Divisjon +
          '},null]},';
        teller++;
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringDivisjonerPieChart = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"divisjon", "label":"Divisjon", "type":"string", "p":{}},' +
        '{"id":"sesong", "label":"Sesong", "type":"number", "p":{}}],' +
        ' "rows": [';
      var tabell = new Array(9);
      for (var i = 0; i < tabell.length; i++) {
        tabell[i] = 0;
      }
      for (var i = 0; i < data.length; i++) {
        var divisjon = data[i].Divisjon;
        tabell[divisjon - 1] += 1;
      }
      for (var i = 0; i < tabell.length; i++) {
        if (tabell[i] != 0) {
          var forekomsterDivisjon = tabell[i];
          var divisjon = i + 1;
          string += '{"c":[{"v":"Divisjon ' + divisjon + '"},{"v":' + forekomsterDivisjon +
            '},null]},';
        }
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagStringKampResultaterPieChart = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"id":"kamp", "label":"Kamp", "type":"string", "p":{}},' +
        '{"id":"antall", "label":"Antall", "type":"number", "p":{}}' +
        '],' +
        ' "rows": [';
      //Tabell der index 0=antall tap, 1=antall uavgjort, 2=antall seiere
      var tabell = [0, 0, 0];
      for (var i = 0; i < data.length; i++) {
        tabell[0] += parseInt(data[i].Tapt);
        tabell[1] += parseInt(data[i].Uavgjort);
        tabell[2] += parseInt(data[i].Vunnet);
      }
      /**string += '{"c":[{"v":"Divisjon ' + divisjon + '"},{"v":' + forekomsterDivisjon +
        '},null]},';*/
      $log.info("Seiere: " + tabell[2] + ", uavgjort: " + tabell[1] + ", tap: " + tabell[0]);
      string += '{"c": [' +
        '{"v":"Vunnet"},' +
        '{"v":' + tabell[2] + '}' +
        ']},' +
        '{"c": [' +
        '{"v":"Tapt"},' +
        '{"v":' + tabell[0] + '}' +
        ']},' +
        '{"c": [' +
        '{"v":"Uavgjort"},' +
        '{"v":' + tabell[1] + '}' +
        ']}' +
        ']}';
      $log.info(string);
      return string;
    };

    var onUserComplete = function(data) {
      $rootScope.spiller = data;
      $rootScope.spillerRankingString = lagStringRanking(JSON.parse(data.Ranking));
      $rootScope.kamperString = lagStringKamper(data.Resultater);
      $rootScope.resultatString = lagStringKampResultater(data.Resultater);
      $rootScope.resultatStringUUavgjort = lagStringKampResultaterUUavgjort(data.Resultater);
      $rootScope.resultatStringCombo = lagStringKampResultaterCombo(data.Resultater);
      $rootScope.resultatStringPieChart = lagStringKampResultaterPieChart(data.Resultater);
      $rootScope.divisjonString = lagStringDivisjoner(data.Resultater);
      $rootScope.divisjonStringPieChart = lagStringDivisjonerPieChart(data.Resultater);
      $rootScope.antallKamperOgSeiere = lagStringAntallKamperOgSeiere(data.Resultater);
      googleChartApiPromise.then(buildDataTableKampResultater)
        .then(buildDataTableDivisjon)
        .then(buildDataTableDivisjonPieChart)
        .then(buildDataTableKampResultaterPieChart)
        .then(buildDataTableSpillerRanking);
    };

    var onError = function(reason) {
      $rootScope.error = "Kunne ikke finne spilleren";
    };

    var spillerSoek = function(username) {
      RestService.getSpillerInfo(username).then(onUserComplete, onError);
    };

    // Nullstiller spillerobjektet hvis vi går bort fra viewet for spillerinfo
    // eller spillerdivisjon
    $rootScope.$watch(function() {
      return $location.path();
    }, function(value) {
      if (value != "/spillerinfo" && value != "/spillerdivisjon") {
        $rootScope.spiller = null;
      }
    });

    $rootScope.lineChartSpillerRanking = {
      type: "LineChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Rank",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.areaChartSpillerRanking = {
      type: "AreaChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Rank",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    var antall = $rootScope.antallSesonger;

    $rootScope.stackedColumnChartSpillerSoek = {
      type: "ColumnChart",
      options: {
        isStacked: true,
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.stackedColumnChartSpillerSoekBasic = {
      type: "ColumnChart",
      options: {
        isStacked: true,
        hAxis: {
          title: "Sesong",
        },
        vAxis: {
          title: "Antall spilt",
        }
      }
    };

    $rootScope.columnChartKamper = {
      type: "ColumnChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall kamper",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.columnChartSpillerSoek = {
      type: "ColumnChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.columnChartUUavgjortSpillerSoek = {
      type: "ColumnChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.comboChartSpillerSoek = {
      type: "ComboChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        },
        bar: {
          groupWidth: "40%"
        },
        seriesType: 'bars',
        series: {
          2: {
            type: 'line'
          }
        }
      }
    };

    $rootScope.areaChartSpillerSoek = {
      type: "AreaChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.steppedAreaChartSpillerSoek = {
      type: "SteppedAreaChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.lineChartSpillerSoek = {
      type: "LineChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: '#f2f2f2'
          }
        }
      }
    };

    $rootScope.scatterChartSpillerSoek = {
      type: "ScatterChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'top',
          alignment: 'center'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall spilt",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    $rootScope.columnChartDivisjon = {
      type: "ColumnChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        title: "Column chart",
        legend: {
          position: 'none'
        },
        isStacked: true,
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Divisjon",
          gridlines: {
            color: "#f2f2f2"
          },
          //ticks: divisjonYVerdier
          ticks: [0, 2, 4, 6, 8, 10]
        }
      }
    };

    $rootScope.areaChartDivisjon = {
      type: "AreaChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        title: "Area chart",
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Divisjon",
          gridlines: {
            color: "#f2f2f2"
          },
          ticks: [0, 2, 4, 6, 8, 10]
        }
      }
    };

    $rootScope.steppedAreaChartDivisjon = {
      type: "SteppedAreaChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        title: "Stepped Area Chart",
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Divisjon",
          gridlines: {
            color: "#f2f2f2"
          },
          ticks: [0, 2, 4, 6, 8, 10]
        }
      }
    };

    $rootScope.lineChartDivisjon = {
      type: "LineChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Divisjon",
          gridlines: {
            color: "#f2f2f2"
          },
          ticks: [0, 2, 4, 6, 8, 10]
        }
      }
    };

    $rootScope.scatterChartDivisjon = {
      type: "ScatterChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Sesong",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Divisjon",
          gridlines: {
            color: "#f2f2f2"
          },
          ticks: [0, 2, 4, 6, 8, 10]
        }
      }
    };

    $rootScope.pieChartDivisjon = {
      type: "PieChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: "none",
        pieSliceText: "label",
        tooltip: {
          text: "both"
        },
        legend: {
          position: 'top'
        }
      }
    };

    $rootScope.pieChartKampResultater = {
      type: "PieChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        tooltip: {
          text: "both"
        },
        legend: {
          position: 'top',
          alignment: 'center'
        }
      }
    };

    $rootScope.scatterChartKamperOgSeiere = {
      type: "ScatterChart",
      options: {
        colors: ['#12283a', '#3679b0', '#b1cfe7'],
        backgroundColor: {
          fill: '#f2f2f2'
        },
        legend: {
          position: 'none'
        },
        hAxis: {
          title: "Antall seiere",
          maxAlternation: 1,
          slantedText: false
        },
        vAxis: {
          title: "Antall kamper",
          gridlines: {
            color: "#f2f2f2"
          }
        }
      }
    };

    function buildDataTableKampResultater() {
      var resultatObj = JSON.parse($rootScope.resultatString);
      var resultatUUavgjortObj = JSON.parse($rootScope.resultatStringUUavgjort);
      var resultatComboObj = JSON.parse($rootScope.resultatStringCombo);
      var kamperObj = JSON.parse($rootScope.kamperString);
      $rootScope.stackedColumnChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
      $rootScope.stackedColumnChartSpillerSoekBasic.data = new google.visualization.DataTable(resultatObj);
      $rootScope.columnChartKamper.data = new google.visualization.DataTable(kamperObj);
      $rootScope.columnChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
      $rootScope.columnChartUUavgjortSpillerSoek.data = new google.visualization.DataTable(resultatUUavgjortObj);
      $rootScope.comboChartSpillerSoek.data = new google.visualization.DataTable(resultatComboObj);
      $rootScope.areaChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
      $rootScope.steppedAreaChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
      $rootScope.lineChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
      $rootScope.scatterChartSpillerSoek.data = new google.visualization.DataTable(resultatObj);
    };

    function buildDataTableDivisjon() {
      var obj = JSON.parse($rootScope.divisjonString);
      var kamperOgSeiereObj = JSON.parse($rootScope.antallKamperOgSeiere);
      $rootScope.columnChartDivisjon.data = new google.visualization.DataTable(obj);
      $rootScope.areaChartDivisjon.data = new google.visualization.DataTable(obj);
      $rootScope.steppedAreaChartDivisjon.data = new google.visualization.DataTable(obj);
      $rootScope.lineChartDivisjon.data = new google.visualization.DataTable(obj);
      $rootScope.scatterChartDivisjon.data = new google.visualization.DataTable(obj);
      $rootScope.scatterChartKamperOgSeiere.data = new google.visualization.DataTable(kamperOgSeiereObj);
    };

    function buildDataTableDivisjonPieChart() {
      var obj = JSON.parse($rootScope.divisjonStringPieChart);
      $rootScope.pieChartDivisjon.data = new google.visualization.DataTable(obj);
    };

    function buildDataTableKampResultaterPieChart() {
      var obj = JSON.parse($rootScope.resultatStringPieChart);
      $rootScope.pieChartKampResultater.data = new google.visualization.DataTable(obj);
    };

    function buildDataTableSpillerRanking() {
      var obj = JSON.parse($rootScope.spillerRankingString);
      $rootScope.lineChartSpillerRanking.data = new google.visualization.DataTable(obj);
      $rootScope.areaChartSpillerRanking.data = new google.visualization.DataTable(obj);
    };

    return {
      spillerSoek: spillerSoek
    };
  };

  var module = angular.module("minApp");
  module.factory("SpillerSoekService", SpillerSoekService);

}());
