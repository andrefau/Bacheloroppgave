(function() {
    var RankingService = function($rootScope, RestService) {

        var onRanking = function(data) {
            $rootScope.spillereRank = data;
        };

        var onError = function(reason) {
            $rootScope.error = "Kunne ikke finne spillerene";
        };

        var hentRank = function() {
            RestService.getRanking().then(onRanking, onError);
        };

        return {
            hentRank: hentRank
        };
    };

    var module = angular.module("minApp");
    module.factory("RankingService", RankingService);

}());
