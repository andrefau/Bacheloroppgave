(function() {

    var GruppeSannsService = function($rootScope, RestService, googleChartApiPromise) {

      var lagString = function(data) {
        var sannsFor = data.Sannsynlighet;
        var sannsMot = 100-data.Sannsynlighet;
          var string = '{' +
                '"cols": [' +
                      '{"label":"Navn","type":"string"},' +
                      '{"label":"Sannsynlighet","type":"number"}' +
                    '],' +
               ' "rows": [{"c":[{"v":"Sannsynlighet for førsteplass"},{"v":' + sannsFor + ', "f": "' + sannsFor + '%" }]}, ' +
                        '{"c":[{"v":"Sannsynlighet for annen plassering"},{"v":' + sannsMot + ', "f": "' + sannsMot +'%"}]}]}';
          return string;
      };

      var hentGruppeSanns = function(navn) {
          RestService.getGruppeSanns(navn).then(onGruppeSannsComplete, onError);
      };

      var onGruppeSannsComplete = function(data) {
        $rootScope.gruppeSanns = lagString(data);
        $rootScope.gruppeSannsResponse = data;
        googleChartApiPromise.then(buildDataTable);
      };

      var onError = function(reason) {
        $rootScope.error = "Kunne ikke finne dataene.";
      };

      // Definerer ulike charts
      $rootScope.columnChartGruppeSanns = {
          type: "ColumnChart",
          options: {
              legend: { position: 'none' },
              vAxis: {
                title: "Sannsynlighet"
              }
          }
      };

      $rootScope.pieChartGruppeSanns = {
          type: "PieChart",
          options: {
            tooltip: {
              text: 'value'
            }
          }
      };

      $rootScope.barChartGruppeSanns = {
          type: "BarChart",
          options: {
              legend: { position: 'none' },
              hAxis: {
                  title: "Sannsynlighet"
              }
          }
      };

      function buildDataTable() {
          var obj = JSON.parse($rootScope.gruppeSanns);
          $rootScope.columnChartGruppeSanns.data = new google.visualization.DataTable(obj);
          $rootScope.pieChartGruppeSanns.data = new google.visualization.DataTable(obj);
          $rootScope.barChartGruppeSanns.data = new google.visualization.DataTable(obj);
      };

      return {
          hentGruppeSanns: hentGruppeSanns
      };

    };

    var module = angular.module("minApp");
    module.factory("GruppeSannsService", GruppeSannsService);

  }());
