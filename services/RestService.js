(function() {

    var RestService = function($http, $log) {

        // Kode for å hente alle spillere med rank
        var getRanking = function() {
            //return $http.get("http://localhost:9995/ranking")                 // Lokal server
            return $http.get("http://82.196.1.230:8080/wloh/api/ranking")   // Remote server
                .then(function(response) {
                    return response.data;
                });
        };

        // Kode for å hente en liste over antall spillere pr sesong
        var getAlleSpillere = function() {
            //return $http.get("http://localhost:9995/antallspillerepersesong")               // Lokal server
            return $http.get("http://82.196.1.230:8080/wloh/api/antallspillerepersesong") // Remote server
                .then(function(response) {
                    return response.data;
                });
        };

        // Kode for å hente en liste over antall uavgjort per sesong
        var getAntallUavgjort = function() {
          //return $http.get("http://localhost:9995/antalluavgjortpersesong")               // Lokal server
          return $http.get("http://82.196.1.230:8080/wloh/api/antalluavgjortpersesong") // Remote server
              .then(function(response) {
                  return response.data;
              });
        };

        // Kode for å hente generell statistikk for hver sesong
        var getStatistikk = function() {
            return $http.get("http://82.196.1.230:8080/wloh/api/statistikk")    // Remote server
                .then(function(response) {
                    return response.data;
                });
        };

        // Kode for å hente info om en spiller
        var getSpillerInfo = function(navn) {
            $log.info("Prøver å finne " +navn);
            //return $http.get("http://localhost:9995/spillere/" +navn)                   // Lokal server
            return $http.get("http://82.196.1.230:8080/wloh/api/spillere/" +navn)     // Remote server
                .then(function(response) {
                    $log.info("Returnert spiller: " +response.data);
                    return response.data;
                });
        };

        var getKampSanns = function(navn1, navn2) {
            // Kode for å hente sannsynlighet for at en spiller
            // vinner over en annen spiller
            $log.info("Søker etter vinnersannsynlighet for " + navn1 + " mot " + navn2 + ".");
            //return $http.get("http://localhost:9995/vinnersannsynlighet/" + navn1 + "/" + navn2)                // Lokal server
            return $http.get("http://82.196.1.230:8080/wloh/api/vinnersannsynlighet/" + navn1 + "/" + navn2)  // Remote server
            .then(function(response) {
              $log.info("Returnert sannsynlighet: " + response.data);
              return response.data;
            });
        };

        var getGruppeSanns = function(navn) {
            // Kode for å hente sannsynlighet for at en spiller
            // vinner i sin gruppe
            $log.info("Søker etter vinnersannsynlighet for " + navn + " i gruppe.");
            //return $http.get("http://localhost:9995/sannsynlighetgruppeseier/" + navn)                  // Lokal server
            return $http.get("http://82.196.1.230:8080/wloh/api/sannsynlighetgruppeseier/" + navn)    // Remote server
            .then(function(response) {
              $log.info("Returnert sannsynlighet: " + response.data);
              return response.data;
            });
        };

        return {
            getRanking: getRanking,
            getAlleSpillere: getAlleSpillere,
            getAntallUavgjort: getAntallUavgjort,
            getSpillerInfo: getSpillerInfo,
            getKampSanns: getKampSanns,
            getGruppeSanns: getGruppeSanns,
            getStatistikk: getStatistikk
        };
    };

    var module = angular.module("minApp");
    module.factory("RestService", RestService);

}());
