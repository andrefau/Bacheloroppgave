(function() {

    var StatistikkService = function($rootScope, RestService, googleChartApiPromise) {

        var lagAntallSpillerePerSesongString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"Sesong","type":"string"},' +
                        '{"label":"Antall spillere","type":"number"}' +
                      '],' +
                 ' "rows": [';
            for (var i in data) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_spillere+ '}, null]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var lagAntallUavgjortPerSesongString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"Sesong","type":"string"},' +
                        '{"label":"Antall uavgjort","type":"number"}' +
                      '],' +
                 ' "rows": [';
            for (var i in data) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_uavgjort+ '}]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var lagAntallDivisjonerPerSesongString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"Sesong", "type":"string"},' +
                        '{"label":"Divisjoner", "type":"number"}' +
                      '],' +
                 ' "rows": [';
            for (var i in data) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_divisjoner+ '}]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var lagAntallGrupperPerSesongString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"Sesong", "type":"string"},' +
                        '{"label":"Grupper", "type":"number"}' +
                      '],' +
                 ' "rows": [';
            for (var i in data) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_grupper+ '}]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var lagAntallWFPoengPrSesongString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"Sesong","type":"string"},' +
                        '{"label":"Antall wordfeudpoeng","type":"number"}' +
                      '],' +
                 ' "rows": [';
            for (var i in data) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].antall_wfpoeng+ '}]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var lagSamletStatistikkString = function(data) {
            var string = '{' +
                  '"cols": [' +
                        '{"label":"ID","type":"string"},' +
                        '{"label":"Sesong", "type":"number"},' +
                        '{"label":"Grupper", "type":"number"},' +
                        '{"label":"Divisjoner", "type":"string"},' +
                        '{"label":"Antall wordfeudpoeng","type":"number"}' +
                      '],' +
                 ' "rows": [';
            //for (var i in data) {
            for(var i = 9; i < data.length; i+=10) {
                string += '{"c":[{"v":"' +data[i].sesong+ '"},{"v":' +data[i].sesong+
                '},{"v":"' +data[i].antall_grupper+ '"},{"v":"' +data[i].antall_divisjoner+ '"},{"v":"' +data[i].antall_wfpoeng+ '"}]},';
            }
            string = string.slice(0, -1);
            string += ']}';
            return string;
        };

        var onError = function(reason) {
            $rootScope.statistikkServiceError = "Kunne ikke finne dataene.";
        };

        var onAntallSpillerePerSesong = function(data) {
            $rootScope.antallSpillerePerSesong = lagAntallSpillerePerSesongString(data);
            googleChartApiPromise.then(buildAntallSpillerePerSesongTable);
        };

        var onAntallUavgjortPerSesong = function(data) {
            $rootScope.antallUavgjortPerSesong = lagAntallUavgjortPerSesongString(data);
            googleChartApiPromise.then(buildAntallUavgjortPerSesongTable);
        };

        var onGenerellStatistikkPerSesong = function(data) {
            $rootScope.antallDivisjonerPerSesong = lagAntallDivisjonerPerSesongString(data);
            $rootScope.antallGrupperPerSesong = lagAntallGrupperPerSesongString(data);
            $rootScope.antallWFPoengPerSesong = lagAntallWFPoengPrSesongString(data);
            $rootScope.samletStatistikk = lagSamletStatistikkString(data);
            $rootScope.samletStatistikkTabell = data;

            googleChartApiPromise.then(buildAntallDivisjonerPerSesongTable)
                                 .then(buildAntallGrupperPerSesongTable)
                                 .then(buildAntallWFPoengPerSesongTable)
                                 .then(buildSamletStatistikkTable);
        };

        var hentGenerellStatistikkPerSesong = function() {
            RestService.getStatistikk().then(onGenerellStatistikkPerSesong, onError);
        };

        var hentAntallSpillerePerSesong = function() {
            RestService.getStatistikk().then(onAntallSpillerePerSesong, onError);
        };

        var hentAntallUavgjortPerSesong = function() {
            RestService.getStatistikk().then(onAntallUavgjortPerSesong, onError);
        };

        // Definerer ulike charts for antall spillere pr sesong
        $rootScope.lineChartAntallSpillerePerSesong = {
            type: "LineChart",
            options: {
                curveType: "function",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall spillere"
                }
            }
        };

        $rootScope.columnChartAntallSpillerePerSesong = {
            type: "ColumnChart",
            options: {
                title: "Column chart",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall spillere"
                }
            }
        };

        $rootScope.areaChartAntallSpillerePerSesong = {
          type: "AreaChart",
          options: {
            title: "Area chart",
            legend: { position: 'none' },
            hAxis: {
                title: "Sesong"
            },
            vAxis: {
                title: "Antall spillere"
            }
          }
        };

        $rootScope.steppedAreaChartAntallSpillerePerSesong = {
          type: "SteppedAreaChart",
          options: {
              title: "Stepped Area Chart",
              legend: { position: 'none' },
              hAxis: {
                  title: "Sesong"
              },
              vAxis: {
                  title: "Antall spillere"
              }
          }
        };

        $rootScope.histogramAntallSpillerePerSesong = {
            type: "Histogram",
            options: {
                title: "Histogram",
                legend: { position: 'none' },
                hAxis: {
                    title: "Antall spillere"
                },
                vAxis: {
                    title: "Antall sesonger"
                }
            }
        };

        // Definerer ulike charts for antall uavgjort pr sesong
        $rootScope.lineChartAntallUavgjortPerSesong = {
            type: "LineChart",
            options: {
                title: "Line chart",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall uavgjort",
                    viewWindowMode: "explicit",
                    viewWindow:{ min: 0 }
                }
            }
        };

        $rootScope.columnChartAntallUavgjortPerSesong = {
            type: "ColumnChart",
            options: {
                title: "Column chart",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall uavgjort"
                }
            }
        };

        $rootScope.pieChartAntallUavgjortPerSesong = {
            type: "PieChart",
            options: {
                title: "Pie chart",
                pieHole: 0.4
            }
        };

        $rootScope.areaChartAntallUavgjortPerSesong = {
          type: "AreaChart",
          options: {
            title: "Area chart",
            legend: { position: 'none' },
            hAxis: {
                title: "Sesong"
            },
            vAxis: {
                title: "Antall uavgjort"
            }
          }
        };

        $rootScope.steppedAreaChartAntallUavgjortPerSesong = {
          type: "SteppedAreaChart",
          options: {
              title: "Stepped Area Chart",
              legend: { position: 'none' },
              hAxis: {
                  title: "Sesong"
              },
              vAxis: {
                  title: "Antall uavgjort"
              }
          }
        };

        $rootScope.scatterChartAntallUavgjortPerSesong = {
            type: "ScatterChart",
            options: {
                title: "Scatter chart",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall uavgjort"
                }
            }
        };

        // Definerer ulike charts for generell statistikk pr sesong
        $rootScope.lineChartAntallDivisjonerPerSesong = {
            type: "LineChart",
            options: {
                title: "Antall divisjoner pr sesong",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall divisjoner",
                    ticks: [0,2,4,6,8,10]
                }
            }
        };

        $rootScope.columnChartAntallDivisjonerPerSesong = {
            type: "ColumnChart",
            options: {
                title: "Antall divisjoner pr sesong",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall divisjoner",
                    ticks: [0,2,4,6,8,10]
                }
            }
        };

        $rootScope.areaChartAntallDivisjonerPerSesong = {
            type: "AreaChart",
            options: {
                title: "Antall divisjoner pr sesong",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall divisjoner",
                    ticks: [0,2,4,6,8,10]
                }
            }
        };

        $rootScope.lineChartAntallGrupperPerSesong = {
            type: "LineChart",
            options: {
                title: "Antall grupper pr sesong",
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall grupper"
                }
            }
        };

        $rootScope.lineChartAntallWFPoengPerSesong = {
            type: "LineChart",
            options: {
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall wordfeudpoeng"
                }
            }
        };

        $rootScope.areaChartAntallWFPoengPerSesong = {
            type: "AreaChart",
            options: {
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall wordfeudpoeng"
                }
            }
        };

        $rootScope.columnChartAntallWFPoengPerSesong = {
            type: "ColumnChart",
            options: {
                legend: { position: 'none' },
                hAxis: {
                    title: "Sesong"
                },
                vAxis: {
                    title: "Antall wordfeudpoeng"
                }
            }
        };

        $rootScope.bubbleChartSamletStatistikk = {
            type: "BubbleChart",
            options: {
                title: "Samlet statisikk",
                hAxis: {
                    title: "Sesong",
                    ticks: [0,25,50,75,100,125]
                },
                vAxis: {
                    title: "Grupper"
                },
                bubble: {
                    textStyle: {
                        fontSize: 11
                    }
                }
            }
        };

        function buildAntallSpillerePerSesongTable() {
            var obj = JSON.parse($rootScope.antallSpillerePerSesong);
            $rootScope.lineChartAntallSpillerePerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.columnChartAntallSpillerePerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.areaChartAntallSpillerePerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.steppedAreaChartAntallSpillerePerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.histogramAntallSpillerePerSesong.data = new google.visualization.DataTable(obj);
        };

        function buildAntallUavgjortPerSesongTable() {
            var obj = JSON.parse($rootScope.antallUavgjortPerSesong);
            $rootScope.lineChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.columnChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.pieChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.areaChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.steppedAreaChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.scatterChartAntallUavgjortPerSesong.data = new google.visualization.DataTable(obj);
        };

        function buildAntallDivisjonerPerSesongTable() {
            var obj = JSON.parse($rootScope.antallDivisjonerPerSesong);
            $rootScope.lineChartAntallDivisjonerPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.columnChartAntallDivisjonerPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.areaChartAntallDivisjonerPerSesong.data = new google.visualization.DataTable(obj);
        };

        function buildAntallGrupperPerSesongTable() {
            var obj = JSON.parse($rootScope.antallGrupperPerSesong);
            $rootScope.lineChartAntallGrupperPerSesong.data = new google.visualization.DataTable(obj);
        };

        function buildAntallWFPoengPerSesongTable() {
            var obj = JSON.parse($rootScope.antallWFPoengPerSesong);
            $rootScope.lineChartAntallWFPoengPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.areaChartAntallWFPoengPerSesong.data = new google.visualization.DataTable(obj);
            $rootScope.columnChartAntallWFPoengPerSesong.data = new google.visualization.DataTable(obj);
        };

        function buildSamletStatistikkTable() {
            var obj = JSON.parse($rootScope.samletStatistikk);
            $rootScope.bubbleChartSamletStatistikk.data = new google.visualization.DataTable(obj);
        };

        return {
            hentAntallSpillerePerSesong: hentAntallSpillerePerSesong,
            hentAntallUavgjortPerSesong: hentAntallUavgjortPerSesong,
            hentGenerellStatistikkPerSesong: hentGenerellStatistikkPerSesong
        };
    };

    var module = angular.module("minApp");
    module.factory("StatistikkService", StatistikkService);

}());
