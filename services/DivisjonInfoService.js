(function() {

  var DivisjonInfoService = function($rootScope, $log, RestService, googleChartApiPromise) {

    var lagAntallGrupperPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall grupper","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_grupper + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallGrupperPerDivisjonParetoString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall grupper","type":"number"},' +
        '{"label":"Akkumulert antall","type":"number"}' +
        '],' +
        ' "rows": [';
        var total = 0;
      for (var i in data) {
        total += data[i].antall_grupper;
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_grupper + '},{"v":' + total + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallGrupperPerDivisjonPieChartString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall grupper","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '. divisjon"},{"v":' + data[i].antall_grupper + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallSpillerePerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall spillere","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_spillere + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallSpillereOgPoengPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Antall wordfeudpoeng","type":"string"},' +
        '{"label":"Antall spillere","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].antall_spillere + '"},{"v":' + data[i].antall_wfpoeng + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallWFPoengPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall wordfeudpoeng","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_wfpoeng + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallUavgjortPerDivisjonString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Divisjon","type":"string"},' +
        '{"label":"Antall uavgjort","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].divisjon + '"},{"v":' + data[i].antall_uavgjort + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallSpillerePerGruppeString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Gruppe","type":"string"},' +
        '{"label":"Antall spillere","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].gruppe + '"},{"v":' + data[i].antall_spillere + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallWFPoengPerGruppeString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Gruppe","type":"string"},' +
        '{"label":"Antall wordfeudpoeng","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].gruppe + '"},{"v":' + data[i].antall_wfpoeng + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var lagAntallUavgjortPerGruppeString = function(data) {
      var string = '{' +
        '"cols": [' +
        '{"label":"Gruppe","type":"string"},' +
        '{"label":"Antall uavgjort","type":"number"}' +
        '],' +
        ' "rows": [';
      for (var i in data) {
        string += '{"c":[{"v":"' + data[i].gruppe + '"},{"v":' + data[i].antall_uavgjort + '}, null]},';
      }
      string = string.slice(0, -1);
      string += ']}';
      return string;
    };

    var onError = function(reason) {
      $rootScope.divisjonInfoServiceError = "Kunne ikke finne dataene.";
    };

    var onDivisjonInfo = function(data) {
      $rootScope.divisjonInfo = data[111].divisjoner;
      $rootScope.antallGrupperPerDivisjon = lagAntallGrupperPerDivisjonString(data[111].divisjoner);
      $rootScope.antallGrupperPerDivisjonPareto = lagAntallGrupperPerDivisjonParetoString(data[111].divisjoner);
      $rootScope.antallGrupperPerDivisjonPieChart = lagAntallGrupperPerDivisjonPieChartString(data[111].divisjoner);
      $rootScope.antallSpillerePerDivisjon = lagAntallSpillerePerDivisjonString(data[111].divisjoner);
      $rootScope.antallWFPoengPerDivisjon = lagAntallWFPoengPerDivisjonString(data[111].divisjoner);
      $rootScope.antallSpillereOgPoengPerDivisjon = lagAntallSpillereOgPoengPerDivisjonString(data[111].divisjoner);
      $rootScope.antallUavgjortPerDivisjon = lagAntallUavgjortPerDivisjonString(data[111].divisjoner);

      $rootScope.gruppeInfo = JSON.parse(data[111].divisjoner[8].grupper);
      $rootScope.antallSpillerePerGruppe = lagAntallSpillerePerGruppeString(JSON.parse(data[111].divisjoner[8].grupper));
      $rootScope.antallWFPoengPerGruppe = lagAntallWFPoengPerGruppeString(JSON.parse(data[111].divisjoner[8].grupper));
      $rootScope.antallUavgjortPerGruppe = lagAntallUavgjortPerGruppeString(JSON.parse(data[111].divisjoner[8].grupper));
      googleChartApiPromise.then(buildDivisjonInfoTables)
        .then(buildGruppeInfoTables);
    };

    var hentDivisjonInfo = function() {
      RestService.getStatistikk().then(onDivisjonInfo, onError);
    };

    /* -------- Antall grupper per divisjon -------- */
    $rootScope.lineChartAntallGrupperPerDivisjon = {
      type: "LineChart",
      options: {
        title: "Antall grupper per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall grupper"
        }
      }
    };

    $rootScope.columnChartAntallGrupperPerDivisjon = {
      type: "ColumnChart",
      options: {
        title: "Antall grupper per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall grupper"
        }
      }
    };

    $rootScope.comboChartAntallGrupperPerDivisjonPareto = {
      type: "ComboChart",
      options: {
        title: "Antall grupper per divisjon",
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall grupper"
        },
        legend: { position: "right" },
        seriesType: 'bars',
        series: {1: {type: 'line'}}
      }
    };

    $rootScope.barChartAntallGrupperPerDivisjon = {
      type: "BarChart",
      options: {
        title: "Antall grupper per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Antall grupper"
        },
        vAxis: {
          title: "Divisjon"
        }
      }
    };

    $rootScope.pieChartAntallGrupperPerDivisjon = {
      type: "PieChart",
      options: {
        title: "Antall grupper per divisjon",
        pieSliceText: "label",
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall grupper"
        }
      }
    };

    $rootScope.scatterChartAntallGrupperPerDivisjon = {
      type: "ScatterChart",
      options: {
        title: "Antall grupper per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall grupper"
        }
      }
    };

    /* -------- Antall spillere per divisjon -------- */
    $rootScope.lineChartAntallSpillerePerDivisjon = {
      type: "LineChart",
      options: {
        title: "Antall spillere per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall spillere"
        }
      }
    };

    $rootScope.columnChartAntallSpillerePerDivisjon = {
      type: "ColumnChart",
      options: {
        title: "Antall spillere per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall spillere"
        }
      }
    };

    /* -------- Antall wfpoeng per divisjon -------- */
    $rootScope.lineChartAntallWFPoengPerDivisjon = {
      type: "LineChart",
      options: {
        title: "Antall wordfeudpoeng per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    $rootScope.columnChartAntallWFPoengPerDivisjon = {
      type: "ColumnChart",
      options: {
        title: "Antall wordfeudpoeng per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    /* -------- Antall wfpoeng og spillere per divisjon -------- */

    $rootScope.scatterChartAntallSpillereOgPoengPerDivisjon = {
      type: "ScatterChart",
      options: {
        hAxis: {
          title: "Antall spillere per divisjon"
        },
        vAxis: {
          title: "Antall wordfeudpoeng per divisjon"
        }
      }
    };

    /* -------- Antall uavgjort per divisjon -------- */
    $rootScope.lineChartAntallUavgjortPerDivisjon = {
      type: "LineChart",
      options: {
        title: "Antall uavgjort per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall uavgjort"
        }
      }
    };

    $rootScope.columnChartAntallUavgjortPerDivisjon = {
      type: "ColumnChart",
      options: {
        title: "Antall uavgjort per divisjon",
        legend: { position: 'none' },
        hAxis: {
          title: "Divisjon"
        },
        vAxis: {
          title: "Antall uavgjort"
        }
      }
    };

    /* -------- Antall spillere per gruppe -------- */
    $rootScope.lineChartAntallSpillerePerGruppe = {
      type: "LineChart",
      options: {
        title: "Antall spillere per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall spillere"
        }
      }
    };

    $rootScope.columnChartAntallSpillerePerGruppe = {
      type: "ColumnChart",
      options: {
        title: "Antall spillere per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall spillere"
        }
      }
    };

    /* -------- Antall wfpoeng per gruppe -------- */
    $rootScope.lineChartAntallWFPoengPerGruppe = {
      type: "LineChart",
      options: {
        title: "Antall wordfeudpoeng per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    $rootScope.areaChartAntallWFPoengPerGruppe = {
      type: "AreaChart",
      options: {
        title: "Antall wordfeudpoeng per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    $rootScope.columnChartAntallWFPoengPerGruppe = {
      type: "ColumnChart",
      options: {
        title: "Antall wordfeudpoeng per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    $rootScope.histogramAntallWFPoengPerGruppe = {
      type: "Histogram",
      options: {
        legend: { position: 'none' },
        title: "Antall wordfeudpoeng per gruppe",
        hAxis: {
          title: "Antall wordfeudpoeng",
          maxAlternation: 1
        },
        vAxis: {
          title: "Antall grupper"
        }
      }
    };

    $rootScope.scatterChartAntallWFPoengPerGruppe = {
      type: "ScatterChart",
      options: {
        title: "Antall wordfeudpoeng per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall wordfeudpoeng"
        }
      }
    };

    /* -------- Antall uavgjort per gruppe -------- */
    $rootScope.lineChartAntallUavgjortPerGruppe = {
      type: "LineChart",
      options: {
        title: "Antall uavgjort per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall uavgjort"
        }
      }
    };

    $rootScope.columnChartAntallUavgjortPerGruppe = {
      type: "ColumnChart",
      options: {
        title: "Antall uavgjort per gruppe",
        legend: { position: 'none' },
        hAxis: {
          title: "Gruppe"
        },
        vAxis: {
          title: "Antall uavgjort"
        }
      }
    };

    function buildDivisjonInfoTables() {
      var antGrupper = JSON.parse($rootScope.antallGrupperPerDivisjon);
      var antGrupperPareto = JSON.parse($rootScope.antallGrupperPerDivisjonPareto);
      var antGrupperPieChart = JSON.parse($rootScope.antallGrupperPerDivisjonPieChart);
      var antSpillere = JSON.parse($rootScope.antallSpillerePerDivisjon);
      var antWFPoeng = JSON.parse($rootScope.antallWFPoengPerDivisjon);
      var antUavgjort = JSON.parse($rootScope.antallUavgjortPerDivisjon);
      var antSpillereOgPoeng = JSON.parse($rootScope.antallSpillereOgPoengPerDivisjon);
      $rootScope.lineChartAntallGrupperPerDivisjon.data = new google.visualization.DataTable(antGrupper);
      $rootScope.columnChartAntallGrupperPerDivisjon.data = new google.visualization.DataTable(antGrupper);
      $rootScope.comboChartAntallGrupperPerDivisjonPareto.data = new google.visualization.DataTable(antGrupperPareto);
      $rootScope.barChartAntallGrupperPerDivisjon.data = new google.visualization.DataTable(antGrupper);
      $rootScope.pieChartAntallGrupperPerDivisjon.data = new google.visualization.DataTable(antGrupperPieChart);
      $rootScope.scatterChartAntallGrupperPerDivisjon.data = new google.visualization.DataTable(antGrupper);
      $rootScope.lineChartAntallSpillerePerDivisjon.data = new google.visualization.DataTable(antSpillere);
      $rootScope.columnChartAntallSpillerePerDivisjon.data = new google.visualization.DataTable(antSpillere);
      $rootScope.lineChartAntallWFPoengPerDivisjon.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.columnChartAntallWFPoengPerDivisjon.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.scatterChartAntallSpillereOgPoengPerDivisjon.data = new google.visualization.DataTable(antSpillereOgPoeng);
      $rootScope.lineChartAntallUavgjortPerDivisjon.data = new google.visualization.DataTable(antUavgjort);
      $rootScope.columnChartAntallUavgjortPerDivisjon.data = new google.visualization.DataTable(antUavgjort);
    };

    function buildGruppeInfoTables() {
      var antSpillere = JSON.parse($rootScope.antallSpillerePerGruppe);
      var antWFPoeng = JSON.parse($rootScope.antallWFPoengPerGruppe);
      var antUavgjort = JSON.parse($rootScope.antallUavgjortPerGruppe);
      $rootScope.lineChartAntallSpillerePerGruppe.data = new google.visualization.DataTable(antSpillere);
      $rootScope.columnChartAntallSpillerePerGruppe.data = new google.visualization.DataTable(antSpillere);
      $rootScope.lineChartAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.areaChartAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.columnChartAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.histogramAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.scatterChartAntallWFPoengPerGruppe.data = new google.visualization.DataTable(antWFPoeng);
      $rootScope.lineChartAntallUavgjortPerGruppe.data = new google.visualization.DataTable(antUavgjort);
      $rootScope.columnChartAntallUavgjortPerGruppe.data = new google.visualization.DataTable(antUavgjort);
    };

    return {
      hentDivisjonInfo: hentDivisjonInfo
    };
  };

  var module = angular.module("minApp");
  module.factory("DivisjonInfoService", DivisjonInfoService);

}());
