(function() {

    var KampSannsService = function($rootScope, RestService, googleChartApiPromise) {

      var lagString = function(data) {
        var sannsFor = data.Sannsynlighet;
        var sannsMot = (100-data.Sannsynlighet);
          var string = '{' +
                '"cols": [' +
                      '{"label":"Navn","type":"string"},' +
                      '{"label":"Sannsynlighet","type":"number"}' +
                    '],' +
               ' "rows": [{"c":[{"v":"Sannsynlighet for seier"},{"v":' + sannsFor + ', "f": "' + sannsFor + '%"}]}, ' +
                        '{"c":[{"v":"Sannsynlighet for tap"},{"v":' + sannsMot + ', "f": "' + sannsMot + '%"}]}]}';
          return string;
      };

      var hentKampSanns = function(navn1, navn2) {
          RestService.getKampSanns(navn1, navn2).then(onKampSannsComplete, onError);
      };

      var onKampSannsComplete = function(data) {
        $rootScope.kampSanns = lagString(data);
        $rootScope.kampSannsResponse = data;
        googleChartApiPromise.then(buildDataTable);
      };

      var onError = function(reason) {
        $rootScope.errorKampSanns = "Kunne ikke finne dataene.";
      };

      // Definerer ulike charts
      $rootScope.columnChartKampSanns = {
          type: "ColumnChart",
          options: {
            title: "Column chart",
            legend: { position: 'none' },
            vAxis: {
                title: "Sannsynlighet"
            }
          }
      };

      $rootScope.pieChartKampSanns = {
          type: "PieChart",
          tooltip: {textStyle: {color: '#FF0000'}, showColorCode: true},
          options: {
            title: "Pie chart",
            tooltip: {text: 'value'}
          }
      };

      $rootScope.barChartKampSanns = {
          type: "BarChart",
          options: {
              title: "Bar chart",
              legend: { position: 'none' },
              hAxis: {
                  title: "Sannsynlighet"
              }
          }
      };

      function buildDataTable() {
          var obj = JSON.parse($rootScope.kampSanns);
          $rootScope.columnChartKampSanns.data = new google.visualization.DataTable(obj);
          $rootScope.pieChartKampSanns.data = new google.visualization.DataTable(obj);
          $rootScope.barChartKampSanns.data = new google.visualization.DataTable(obj);
      };

      return {
          hentKampSanns: hentKampSanns
      };

    };

    var module = angular.module("minApp");
    module.factory("KampSannsService", KampSannsService);

  }());
